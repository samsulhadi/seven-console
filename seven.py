from random import shuffle

#Kartu ♠(R) , ♦ (J), ♣ (K), ♥(H)
# kartu = (
# 	'A♠', '2♠', '3♠','4♠', '5♠', '6♠','7♠', '8♠','9♠', '10♠','J♠','Q♠','K♠', 
# 	'A♦', '2♦', '3♦', '4♦', '5♦', '6♦','7♦', '8♦','9♦', '10♦','J♦','Q♦','K♦', 
# 	'A♣', '2♣', '3♣', '4♣', '5♣', '6♣','7♣', '8♣','9♣', '10♣','J♣','Q♣','K♣', 
# 	'A♥', '2♥', '3♥', '4♥', '5♥', '6♥','7♥', '8♥','9♥', '10♥','J♥','Q♥','K♥')

# kartu pasti tidak ada di tabel permainan
def check_kartu(kartu_, tabel_permainan_):

	return True

print("==Selamat Datang di Permainan Seven==")
while True:
	## tuple kartu
	kartu = (
		(1,'A♠'),(2,'2♠'),(3,'3♠'),(4,'4♠'),(5,'5♠'),
		(6,'6♠'),(7,'7♠'),(8,'8♠'),(9,'9♠'),(10,'10♠'),
		(11,'J♠'),(12,'Q♠'),(13,'K♠'),(14,'A♦'),(15,'2♦'),
		(16,'3♦'),(17,'4♦'),(18,'5♦'),(19,'6♦'),(20,'7♦'),
		(21,'8♦'),(22,'9♦'),(23,'10♦'),(24,'J♦'),(25,'Q♦'),
		(26,'K♦'),(27,'A♣'),(28,'2♣'),(29,'3♣'),(30,'4♣'),
		(31,'5♣'),(32,'6♣'),(33,'7♣'),(34,'8♣'),(35,'9♣'),
		(36,'10♣'),(37,'J♣'),(38,'Q♣'),(39,'K♣'),(40,'A♥'),
		(41,'2♥'),(42,'3♥'),(43,'4♥'),(44,'5♥'),(45,'6♥'),
		(46,'7♥'),(47,'8♥'),(48,'9♥'),(49,'10♥'),(50,'J♥'),
		(51,'Q♥'),(52,'K♥')
	)
	## dict kartu
	# kartu = {
	# 	1: 'A♠', 2: '2♠', 3: '3♠', 4: '4♠', 5: '5♠', 6: '6♠', 7: '7♠', 8: '8♠', 9: '9♠', 10: '10♠', 11: 'J♠', 12: 'Q♠', 13: 'K♠', 
	# 	14: 'A♦', 15: '2♦', 16: '3♦', 17: '4♦', 18: '5♦', 19: '6♦', 20: '7♦', 21: '8♦', 22: '9♦', 23: '10♦', 24: 'J♦', 25: 'Q♦', 26: 'K♦', 
	# 	27: 'A♣', 28: '2♣', 29: '3♣', 30: '4♣', 31: '5♣', 32: '6♣', 33: '7♣', 34: '8♣', 35: '9♣', 36: '10♣', 37: 'J♣', 38: 'Q♣', 39: 'K♣', 
	# 	40: 'A♥', 41: '2♥', 42: '3♥', 43: '4♥', 44: '5♥', 45: '6♥', 46: '7♥', 47: '8♥', 48: '9♥', 49: '10♥', 50: 'J♥', 51: 'Q♥', 52: 'K♥'
	# }

	print("##Tekan q untuk keluar atau lainnya untuk memulai permainan...##")
	menu = ""
	while True:
		try:
			menu = input("> ")
			break
		except Exception as e:
			print("Pilih menu yang benar!!!")

	if menu == "q":
		break

	total_kartu = len(kartu)
	print("Jumlah Kartu: "+str(total_kartu))
	print("Urutan Normal: "+str(kartu))
	kartu_acak = list(kartu) # jika kartu berupa tuple
	# kartu_acak = kartu.items() # jika kartu berupa dict
	shuffle(kartu_acak)
	# print("Setelah diacak: "+str(kartu_acak))

	players = {
		1:{
			"nama": "Player 1",
			"kartu": [],
			"mati": []
		},
		2:{
			"nama": "Player 2",
			"kartu": [],
			"mati": []
		},
		3:{
			"nama": "Player 3",
			"kartu": [],
			"mati": []
		},
		4:{
			"nama": "Player 4",
			"kartu": [],
			"mati": []
		}
	}

	# menyimpan kartu yang teracak
	daftar_kartu = [
		[],[],[],[]
	]

	# membagi kartu yang teracak dengan modulo 4 ke dalam list di daftar_kartu
	for i in range(total_kartu):
		if i % 4 == 0:
			# append to kartu1 list
			daftar_kartu[0].append(kartu_acak[i])
		elif i % 4 == 1:
			# append to kartu2 list
			daftar_kartu[1].append(kartu_acak[i])
		elif i % 4 == 2:
			# append to kartu3 list
			daftar_kartu[2].append(kartu_acak[i])
		elif i % 4 == 3:
			# append to player2 list
			daftar_kartu[3].append(kartu_acak[i])
	c = 2
	for i,k in enumerate(daftar_kartu):
		# mengurutkan masing2 kartu yg teracak
		k.sort()
		# print("Kartu "+str(i+1)+": "+str(k))
		# mencari kartu 7♠ kemudian memasukkan daftar kartu yg ada 7♠ ke player 1
		if (7,'7♠') in k:
			players[1]["kartu"] = k
		else:
			players[c]["kartu"] = k
			c = c+1

	# print("Player:")
	# print(players)

	tabel_permainan = {
		"♠":[],
		"♦":[],
		"♣":[],
		"♥":[]
	}

	total_putaran = int(total_kartu / 4)
	for r in range(total_putaran):
		print("== Putaran #"+str(r+1)+" ==")
		for no,p in players.items():
			# tampilkan kartu yang keluar saat ini...
			print("Kartu yang keluar saat ini:")
			for jeniskartu,kartu in tabel_permainan.items():
				print(jeniskartu+": "+",".join(kartu))
			print(p["nama"]+":")
			print(str(p["kartu"]))
			pilihan = 0
			while True:
				try:
					pilihan = int(input("Pilih kartu: "))
					kartu_player_yang_dibawa = dict(p["kartu"])
					if pilihan in kartu_player_yang_dibawa:
						if check_kartu(kartu_player_yang_dibawa[pilihan], tabel_permainan):
							print(kartu_player_yang_dibawa[pilihan])
							# menghapus kartu yg dipilih pada playersnya
							del players[no]['kartu'][players[no]['kartu'].index((pilihan, dict(players[no]['kartu'])[pilihan]))]
						break
					else:
						print("Masukkan angka pilihan dari kartu yang Anda bawa.")
				except Exception as e:
					print("Masukkan Angka!!!")



